Source: libcrypt-openssl-bignum-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-perl-openssl,
               libssl-dev,
               perl-openssl-defaults,
               perl-xs-dev,
               perl:native,
               pkg-config
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcrypt-openssl-bignum-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcrypt-openssl-bignum-perl.git
Homepage: https://metacpan.org/release/Crypt-OpenSSL-Bignum

Package: libcrypt-openssl-bignum-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: Perl module to access OpenSSL multiprecision integer arithmetic libraries
 Presently, many though not all of the arithmetic operations that OpenSSL
 provides are exposed to Perl via Crypt::OpenSSL::Bignum. In addition, this
 module can be used to provide access to bignum values produced by other
 OpenSSL modules, such as key parameters from Crypt::OpenSSL::RSA.
